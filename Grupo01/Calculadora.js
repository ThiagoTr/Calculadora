const fs = require('fs');

const operacoes = ['+', '-', '*', '/'];

//Percorre a linha até encontrar a posição da operação 
function quebrarLinha(linha)
{
    let i = 0, opIndex = -1;
    for( ; i < operacoes.length && opIndex === -1; i++)
        opIndex = linha.indexOf(operacoes[i]);

    return [
        linha.substring(0, opIndex).trim(),
        operacoes[i - 1],
        linha.substring(opIndex + 1).trim()
    ];
}

// Leitura do arquivo de entrada
fs.readFile('entrada.txt', 'utf8', (err, data) =>
{
    if(err)
        throw err;

    // Divisão do arquivo em linhas e processamento das operações
    const linhas = data.trim().split('\n');
    const resultados = linhas.map((linha) =>
    {
        const [num1, operacao, num2] = quebrarLinha(linha);
        let resultado;
        switch (operacao)
        {
            case '+':
                resultado = Number(num1) + Number(num2);
                break;
            case '-':
                resultado = Number(num1) - Number(num2);
                break;
            case '*':
                resultado = Number(num1) * Number(num2);
                break;
            case '/':
                resultado = Number(num1) / Number(num2);
                break;
            default:
                throw new Error(`Operação inválida: ${operacao}`);
        }
        return resultado;
    });

    // Escrita do arquivo de saída com nome único
    const dataSaida = resultados.join('\n');
    const nomeSaida = `resultado.txt`;
    fs.writeFile(nomeSaida, dataSaida, (err) =>
    {
        if(err)
            throw err;

        console.log(`Resultados prontos: ${nomeSaida}`);
    });
});