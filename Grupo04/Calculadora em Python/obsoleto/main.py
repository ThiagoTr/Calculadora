from tkinter import *
from tkinter import ttk


entrada = ''
historico = ''
count = 0
resultado = ''
ultEntrada = ''

# cores 
Cor1 = "#0c0d0d" #preto
Cor2 = "#ffffff" #branco
Cor3 = "#e6e6e6" #ZinzaClaro1
Cor4 = "#f70505" #Vermelho
Cor5 = "#8abae0" #AzulClaro
Cor6 = "#f0f0f0" #ZinzaClaro2
Cor7 = "#7bf723" #verde 

#Criando a Janela 
janela = Tk()
janela.title("Calculadora")
janela.geometry("328x487")
janela.config(bg=Cor3)


#criando os frames, uma para o teclado e outro para a tela
frame_tela = Frame(janela, width=330, height=123, bg = Cor3)
frame_tela.grid(row=0, column=0)

frame_corpo = Frame(janela, width=321, height=364, bg = Cor3)
frame_corpo.grid(row=1, column=0)


# crindo label, uma para mostrar as operações sendo
valorTela = StringVar()
valorHist = StringVar()

app_labelTela = Label(frame_tela, textvariable=valorTela, width=40, height=4,bg=Cor3, relief=FLAT, anchor="e", font=('Ivy 9'))
app_labelTela.place(x=0, y=57)

app_labelHist = Label(frame_tela, textvariable=valorHist, width=40, height=1,bg=Cor3, padx=0, relief=FLAT, anchor="e", justify=RIGHT, font=('Ivy 9 bold'))
app_labelHist.place(x=0, y=36)

def entradaValores(x):
    global entrada, historico, count, ultEntrada
    count = count + 1
    entrada = entrada + x
    
    if count == 1 and x in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
        resultado = eval(entrada)
        valorTela.set(resultado)
        entrada = str(resultado)
        count = 0
        if resultado == entrada:
            valorHist.set(entrada)


    elif x not in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
        valorHist.set(entrada)
        count = 0
        
        
    if entrada in ('+', '-', '*', '/', '%'):
        valorHist.set(entrada)

'''    elif x == '=':
        valorHist.set(entrada)
        resultado = eval(entrada)
        valorTela.set(resultado)
        entrada = str(resultado)
    elif x == 'X²':
        resultado = eval(entrada+'**2')
        valorTela.set(resultado)
    elif x == 'C' or x == 'CE' or x == 'DEL' or x == 'C':
        resultado = ''
        valorTela.set(resultado)
    else:
        valorTela.set(entrada)
        resultado = entrada'''

    #valor_texto.set(entrada)
    #resultado = eval(entrada)
    #passando valor para a tela
    #valor_texto.set(resultado)


#criando botões

#Fila0 
# botão 25
b25 = Button(frame_corpo, text="MC", width=7, height=1, bd=0, bg=Cor3, relief=RAISED, overrelief=RIDGE)
b25.place(x=0, y=0)

#botão 26
b26 = Button(frame_corpo, text="MR", width=7, height=1, bd=0, bg=Cor3, relief=RAISED, overrelief=RIDGE)
b26.place(x=54, y=0)

#botão 27
b27 = Button(frame_corpo, text="M+", width=7, height=1, bd=0, bg=Cor3, relief=RAISED, overrelief=RIDGE)
b27.place(x=108, y=0)

#botão 28
b28 = Button(frame_corpo, text="M-", width=7, height=1, bd=0, bg=Cor3, relief=RAISED, overrelief=RIDGE)
b28.place(x=162, y=0)

#botão 29
b29 = Button(frame_corpo, text="MS", width=7, height=1, bd=0, bg=Cor3, relief=RAISED, overrelief=RIDGE)
b29.place(x=216, y=0)

#botão 30
b30 = Button(frame_corpo, text="M", width=7, height=1, bd=0, bg=Cor3, relief=RAISED, overrelief=RIDGE)
b30.place(x=270, y=0)



#Fila1 botão 1
b1 = Button(frame_corpo, command= lambda: entradaValores('%'), text="%", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b1.place(x=0, y=22)

#Fila1 botão 2
b2 = Button(frame_corpo, command= lambda: entradaValores('CE'), text="CE", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b2.place(x=81, y=22)


#Fila1 botão 3
b3 = Button(frame_corpo, command= lambda: entradaValores('C'), text="C", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b3.place(x=162, y=22)

#Fila1 botão 4
b4 = Button(frame_corpo, command= lambda: entradaValores('DEL'), text="DEL", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b4.place(x=243, y=22)




#Fila2 botão 5
b5 = Button(frame_corpo, text="1/x", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b5.place(x=0, y=79)

#Fila2 botão 6
b6 = Button(frame_corpo, command= lambda: entradaValores('X²'), text="X²", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b6.place(x=81, y=79)

#Fila2 botão 7
b7 = Button(frame_corpo, command= lambda: entradaValores('**'), text="√", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b7.place(x=162, y=79)

#Fila2 botão 8
b8 = Button(frame_corpo, command= lambda: entradaValores('/'), text="÷", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b8.place(x=243, y=79)




#Fila3 botão 9
b9 = Button(frame_corpo, command= lambda: entradaValores('7'), text="7", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b9.place(x=0, y=136)

#Fila3 botão 10
b10 = Button(frame_corpo, command= lambda: entradaValores('8'), text="8", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b10.place(x=81, y=136)

#Fila3 botão 11
b11 = Button(frame_corpo, command= lambda: entradaValores('9'), text="9", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b11.place(x=162, y=136)

#Fila3 botão 12
b12 = Button(frame_corpo, command= lambda: entradaValores('*'), text="X", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b12.place(x=243, y=136)



#Fila4 botão 13
b13 = Button(frame_corpo, command= lambda: entradaValores('4'), text="4", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b13.place(x=0, y=193)

#Fila4 botão 14
b14 = Button(frame_corpo, command= lambda: entradaValores('5'), text="5", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b14.place(x=81, y=193)

#Fila4 botão 15
b15 = Button(frame_corpo, command= lambda: entradaValores('6'), text="6", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b15.place(x=162, y=193)

#Fila4 botão 1657
b16 = Button(frame_corpo, command= lambda: entradaValores('-'), text="-", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b16.place(x=243, y=193)



#Fila5 
# botão 17
b17 = Button(frame_corpo, command= lambda: entradaValores('1'), text="1", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b17.place(x=0, y=250)

# botão 18
b18 = Button(frame_corpo, command= lambda: entradaValores('2'), text="2", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b18.place(x=81, y=250)

# botão 19
b19 = Button(frame_corpo, command= lambda: entradaValores('3'), text="3", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b19.place(x=162, y=250)

# botão 20
b20 = Button(frame_corpo, command= lambda: entradaValores('+'), text="+", width=10, height=3, bg=Cor6, relief=RAISED, overrelief=RIDGE)
b20.place(x=243, y=250)



#Fila6
# botão 21
b21 = Button(frame_corpo, text="+/-", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b21.place(x=0, y=307)

# botão 22
b22 = Button(frame_corpo, command= lambda: entradaValores('0'), text="0", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b22.place(x=81, y=307)

# botão 23
b23 = Button(frame_corpo, command= lambda: entradaValores(','), text=",", width=10, height=3, bg=Cor2, relief=RAISED, overrelief=RIDGE)
b23.place(x=162, y=307)

# botão 24
b24 = Button(frame_corpo, command= lambda: entradaValores('='), text="=", width=10, height=3, bg=Cor5, relief=RAISED, overrelief=RIDGE)
b24.place(x=243, y=307)

#entradaValores()

janela.mainloop()

